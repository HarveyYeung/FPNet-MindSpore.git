import mindspore
from mindspore import nn
from mindspore.ops import operations as P
from mindspore.ops import functional as F

class BasicConv2d(nn.Cell):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, groups=1):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, dilation=dilation, pad_mode='pad', has_bias=False, group=groups)
        self.bn = nn.BatchNorm2d(out_planes)
        self.relu = nn.ReLU()

    def construct(self, x):
        x = self.conv(x)
        x = self.bn(x)
        return x



# 基于通道相似度的特征融合CSM2
class TRAM2(nn.Cell):
    def __init__(self, in_channel):
        super(TRAM2, self).__init__()
        self.conv1 = BasicConv2d(in_channel*in_channel, in_channel, 3, 1, 1)
        self.conv2 = BasicConv2d(in_channel, in_channel, 3, 1, 1)
        self.conv3 = BasicConv2d(in_channel, in_channel, 3, 1, 1)
        self.conv4 = BasicConv2d(in_channel, in_channel, 3, 1, 1)


    def construct(self, x1, x2):  # 前面是较浅层特征，后面是修正后的较深层特征(64,22,22) & (64,22,22)
        # print("TRAM")
        N, C, W, H = x1.shape
        x0 = x1
        x0_2 = x2
        x1_ = x1.permute(0, 2, 3, 1)     # (22,22,64)
        x2_ = x2.permute(0, 2, 3, 1)     # (22,22,64)
        x1_ = x1_.reshape(N * H * W, C)  # (22*22,64)
        x2_ = x2_.reshape(N * H * W, C)  # (22*22,64)
        a = x1_.unsqueeze(-1)    # 在最后一维插入一维，变成 (22*22,64,1)
        b = x2_.unsqueeze(-2)    # 在倒数第二维插入一维，变成 (22*22,1,64)
        c = mindspore.ops.bmm(a, b)      # (22*22,64,64)
        c = c.reshape(N, W, H, -1) # (22,22,64*64) ，描述通道相似度
        c = c.permute(0, 3, 1, 2)  # (64*64,22,22)
        c_ = self.conv1(c)         # (64,22,22)
        alpha = self.conv2(c_)     # (64,22,22)
        beta = self.conv3(c_)      # (64,22,22)
        # x_ = self.conv4(x0)
        ret = alpha * x0 + beta + x0_2
        return ret




class FirstOctaveConv(nn.Cell):
    def __init__(self, in_channels, out_channels,kernel_size, alpha=0.5, stride=1, padding=1, dilation=1,
                 groups=1, bias=False):
        super(FirstOctaveConv, self).__init__()
        self.stride = stride
        kernel_size = kernel_size[0]
        self.h2g_pool = nn.AvgPool2d(kernel_size=2, stride=2)
        self.h2l = nn.Conv2d(in_channels, int(alpha * in_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        self.h2h = nn.Conv2d(in_channels, in_channels - int(alpha * in_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)

    def construct(self, x):
        if self.stride ==2:
            x = self.h2g_pool(x)

        X_h2l = self.h2g_pool(x) # 低频，就像GAP是DCT最低频的特殊情况？长宽缩小一半
        X_h = x
        X_h = self.h2h(X_h)
        X_l = self.h2l(X_h2l)

        return X_h, X_l

class OctaveConv(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, alpha=0.5, stride=1, padding=1, dilation=1,
                 groups=1, bias=False):
        super(OctaveConv, self).__init__()
        kernel_size = kernel_size[0]
        self.h2g_pool = nn.AvgPool2d(kernel_size=2, stride=2)
        # self.upsample = torch.nn.Upsample(scale_factor=2, mode='nearest')
        self.upsample =  nn.Upsample(scale_factor=(2.0,2.0), mode='nearest',recompute_scale_factor=True)
        self.stride = stride
        self.l2l = nn.Conv2d(int(alpha * in_channels), int(alpha * out_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        self.l2h = nn.Conv2d(int(alpha * in_channels), out_channels - int(alpha * out_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        self.h2l = nn.Conv2d(in_channels - int(alpha * in_channels), int(alpha * out_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        self.h2h = nn.Conv2d(in_channels - int(alpha * in_channels),
                                   out_channels - int(alpha * out_channels),
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)

    def construct(self, x):
        X_h, X_l = x

        if self.stride == 2:
            X_h, X_l = self.h2g_pool(X_h), self.h2g_pool(X_l)

        X_h2l = self.h2g_pool(X_h)

        X_h2h = self.h2h(X_h)
        X_l2h = self.l2h(X_l)

        X_l2l = self.l2l(X_l)
        X_h2l = self.h2l(X_h2l)

        # X_l2h = self.upsample(X_l2h)
        # print(X_l2h.shape,X_h2h.shape)
        X_l2h = F.interpolate(X_l2h, size  = (int(X_h2h.shape[2]),int(X_h2h.shape[3])), mode='bilinear')
        # print('X_l2h:{}'.format(X_l2h.shape))
        # print('X_h2h:{}'.format(X_h2h.shape))
        X_h = X_l2h + X_h2h
        X_l = X_h2l + X_l2l

        return X_h, X_l

class LastOctaveConv(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, alpha=0.5, stride=1, padding=1, dilation=1,
                 groups=1, bias=False):
        super(LastOctaveConv, self).__init__()
        self.stride = stride
        kernel_size = kernel_size[0]
        self.h2g_pool = nn.AvgPool2d(kernel_size=2, stride=2)

        self.l2h = nn.Conv2d(int(alpha * out_channels), out_channels,
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        self.h2h = nn.Conv2d(out_channels - int(alpha * out_channels),
                                   out_channels,
                                   kernel_size, 1, padding=padding, dilation=dilation, group=groups,  pad_mode='pad', has_bias=bias)
        # self.upsample = torch.nn.Upsample(scale_factor=2, mode='nearest')
        self.upsample =  nn.Upsample(scale_factor=(2.0,2.0), mode='nearest',recompute_scale_factor=True)
    def construct(self, x):
        X_h, X_l = x

        if self.stride == 2:
            X_h, X_l = self.h2g_pool(X_h), self.h2g_pool(X_l)

        X_h2h = self.h2h(X_h) # 高频组对齐通道
        X_l2h = self.l2h(X_l) # 低频组对齐通道
        # 低频组对齐长宽尺寸
        # X_l2h = F.interpolate(X_l2h,  scales = (float(X_h2h.size()[2]), float(X_h2h.size()[3])), mode='bilinear',recompute_scale_factor=True)
        X_l2h = F.interpolate(X_l2h, size  = (int(X_h2h.shape[2]),int(X_h2h.shape[3])), mode='bilinear')
        X_h = X_h2h + X_l2h  # 本来的设置：高频低频融合输出
        return X_h       #都输出


class Octave(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size=(3, 3)):
        super(Octave, self).__init__()
        # 第一层，将特征分为高频和低频
        self.fir = FirstOctaveConv(in_channels, out_channels, kernel_size)
        # 第二层，低高频输入，低高频输出
        self.mid1 = OctaveConv(in_channels, in_channels, kernel_size)
        self.mid2 = OctaveConv(in_channels, out_channels, kernel_size)
        # 第三层，将低高频汇合后输出
        self.lst = LastOctaveConv(in_channels, out_channels, kernel_size)

    def construct(self, x):
        x0 = x
        x_h, x_l = self.fir(x)                   # (1,64,64,64) ,(1,64,32,32)
        x_hh, x_ll = x_h, x_l,
        # x_1 = x_hh +x_ll
        x_h_1, x_l_1 = self.mid1((x_h, x_l))     # (1,64,64,64) ,(1,64,32,32)
        x_h_2, x_l_2 = self.mid1((x_h_1, x_l_1)) # (1,64,64,64) ,(1,64,32,32)
        x_h_5, x_l_5 = self.mid2((x_h_2, x_l_2)) # (1,32,64,64) ,(1,32,32,32)
        x_ret = self.lst((x_h_5, x_l_5)) # (1,64,64,64)
        return x_ret


# 空间注意力模块SAM
class SpatialAttention(nn.Cell):  # 空间注意力模块
    def __init__(self, in_channel):
        super(SpatialAttention, self).__init__()
        self.conv = nn.Conv2d(2, 1, 7, padding=3, pad_mode='pad')
        self.sigmoid = nn.Sigmoid()
        self.mean = P.ReduceMean(keep_dims=True)
        self.max = P.ReduceMax(keep_dims=True)
        self.cat = P.Concat(axis=1)
    def construct(self, x):  # (64,176,176)
        # x维度为 [N, C, H, W] 沿着维度C进行操作, 所以dim=1, 结果为[N, H, W]
        MaxPool = self.max(x,1)  # max 返回的是索引和value， 要用.values去访问值才行！
        AvgPool = self.mean(x,1)  # (176,176)

        # 增加维度, 变成 [N, 1, H, W]
        # MaxPool = mindspore.ops.unsqueeze(MaxPool, dim=1) # (1,176,176)
        # AvgPool = mindspore.ops.unsqueeze(AvgPool, dim=1)
        # 维度拼接 [N, 2, H, W]   # (2,176,176)
        x_cat = self.cat((MaxPool, AvgPool))  # 获得特征图
        # print('SpatialAttention x_cat.shape:',x_cat.shape)
        # 卷积操作得到空间注意力结果
        x_out = self.conv(x_cat) # (1,176,176)
        Ms = self.sigmoid(x_out)

        # 与原图通道进行乘积
        x = Ms * x + x

        return x # (64,176,176)

