import mindspore
from mindspore import nn
from mindspore.ops import operations as P
from mindspore.ops import functional as F
from lib.mymodels import TRAM2  # 特征融合模块SIM
from lib.mymodels import Octave  # 八度卷积
from lib.mymodels import SpatialAttention  # 通道注意力模块SAM
from lib.PVTv1 import pvt_v1

class two_ConvBnRule(nn.Cell):
    def __init__(self, in_chan, out_chan=64):
        super(two_ConvBnRule, self).__init__()

        self.conv1 = nn.Conv2d(
            in_channels=in_chan,
            out_channels=out_chan,
            kernel_size=3,
            padding=1,
            pad_mode='pad'
        )
        self.BN1 = nn.BatchNorm2d(out_chan)
        self.relu1 = nn.ReLU()

        self.conv2 = nn.Conv2d(
            in_channels=out_chan,
            out_channels=out_chan,
            kernel_size=3,
            padding=1,
            pad_mode='pad'
        )
        self.BN2 = nn.BatchNorm2d(out_chan)
        self.relu2 = nn.ReLU()

    def construct(self, x, mid=False):
        feat = self.conv1(x)
        feat = self.BN1(feat)
        feat = self.relu1(feat)
        feat = self.conv2(feat)
        feat = self.BN2(feat)
        feat = self.relu2(feat)
        return feat

class BasicConv2d(nn.Cell):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, relu=False, bn=True):

        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, dilation=dilation, pad_mode='pad', has_bias=False)
        self.bn = nn.BatchNorm2d(out_planes) if bn else None
        self.relu = nn.ReLU() if relu else None

    def construct(self, x):
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        return x

# 解码器
class NeighborConnectionDecoder(nn.Cell):
    def __init__(self, channel):
        super(NeighborConnectionDecoder, self).__init__()
        self.upsample =  nn.Upsample(scale_factor=(2.0,2.0), mode='bilinear',recompute_scale_factor=True)
        # self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)  # 放大两倍
        self.conv_upsample1 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample2 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample3 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample4 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample5 = BasicConv2d(2 * channel, 2 * channel, 3, padding=1)

        self.conv_concat2 = BasicConv2d(2 * channel, 2 * channel, 3, padding=1)
        self.conv_concat3 = BasicConv2d(3 * channel, 3 * channel, 3, padding=1)
        self.conv4 = BasicConv2d(3 * channel, 3 * channel, 3, padding=1)
        self.conv5 = nn.Conv2d(3 * channel, 1, 1)
        self.cat = P.Concat(axis=1)
    def construct(self, x1, x2, x3):  # 深层次特征->浅层特征 f4,f3,f2
        x1_1 = x1
        # f4nc (64*22*22)
        x2_1 = self.conv_upsample1(self.upsample(x1)) * x2  # (64*22*22)*(64*22*22)=(64*22*22)
        # f3nc (64*44*44)
        x3_1 = self.conv_upsample2(self.upsample(x2_1)) * self.conv_upsample3(self.upsample(x2)) * x3  # (64*44*44)
        # concat f4nc和f5nc
        x2_2 = self.cat((x2_1, self.conv_upsample4(self.upsample(x1_1))))  # (128*22*22)
        x2_2 = self.conv_concat2(x2_2)  # (128*22*22)
        # concat f3nc 和 conv(f4nc,f5nc)
        x3_2 = self.cat((x3_1, self.conv_upsample5(self.upsample(x2_2))))  # (192*44*44)
        x3_2 = self.conv_concat3(x3_2)  # (192*44*44)

        x = self.conv4(x3_2)
        x = self.conv5(x)  # (1*44*44)

        return x
# 特征提取模块
class RFB_modified(nn.Cell):
    def __init__(self, in_channel, out_channel):
        super(RFB_modified, self).__init__()
        self.relu = nn.ReLU()
        self.branch0 = nn.SequentialCell(
            BasicConv2d(in_channel, out_channel, 1),
        )
        self.branch1 = nn.SequentialCell(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 3), padding=(0, 0, 1, 1)),
            BasicConv2d(out_channel, out_channel, kernel_size=(3, 1), padding=(1, 1, 0, 0)),
            BasicConv2d(out_channel, out_channel, 3, padding=3, dilation=3)
        )
        self.branch2 = nn.SequentialCell(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 5), padding=(0, 0, 2, 2)),
            BasicConv2d(out_channel, out_channel, kernel_size=(5, 1), padding=(2, 2, 0, 0,)),
            BasicConv2d(out_channel, out_channel, 3, padding=5, dilation=5)
        )
        self.branch3 = nn.SequentialCell(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 7), padding=(0, 0, 3, 3)),
            BasicConv2d(out_channel, out_channel, kernel_size=(7, 1), padding=(3, 3, 0, 0)),
            BasicConv2d(out_channel, out_channel, 3, padding=7, dilation=7)
        )
        self.conv_cat = BasicConv2d(4 * out_channel, out_channel, 3, padding=1)
        self.conv_res = BasicConv2d(in_channel, out_channel, 1)
        self.cat = P.Concat(axis=1)
    def construct(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)
        x_cat = self.conv_cat(self.cat((x0, x1, x2, x3)))

        x = self.relu(x_cat + self.conv_res(x))
        return x

# 我们的网络用的是这一个！！！！！！！！！
class FPM(nn.Cell):
    def __init__(self,channel):
        super(FPM, self).__init__()
        self.tram = TRAM2(channel) # 基于通道注意力的融合模块
        # self.tram = TRAM(channel) # 实验将较浅层的特征加上增强后的较浅层
    def construct(self, x2, pre, x1): # 参数分别表示较深层特征、当前预测图、较浅层特征；x2和pre同size，x1为两倍
        y_1 = x2 * pre
        # y_2 = F.interpolate(y_1, scale_factor=2, mode='bilinear')
        y_2 = F.interpolate(y_1,scale_factor = 2.,
                               mode='bilinear',recompute_scale_factor=True) 
        y_3 = self.tram(x1, y_2) # 前面是较浅层特征
        return y_3





class FPNet(nn.Cell):
    # res2net based encoder decoder
    # imagenet_pretrained为False，不需要下载训练之后网络模型，只是下载一下网络框架
    # imagenet_pretrained为True，需要下载经过训练的网络模型
    def __init__(self, channel=64, imagenet_pretrained=True,img_size=512):
        super(FPNet, self).__init__()
        # ---- PVT Backbone ----
        self.pvt = pvt_v1(pretrained=imagenet_pretrained,img_size=img_size)
        # Octave 消融实验*******************************************************
        self.rfb4_1 = Octave(512, channel)
        self.rfb3_1 = Octave(320, channel)
        self.rfb2_1 = Octave(128, channel)

        # # 特征提取
        self.rfb0_1 = RFB_modified(64, channel)
        # #
        # # 空间注意力模块
        self.sa0 = SpatialAttention(3)

        self.conv2 = two_ConvBnRule(128)
        self.conv3 = two_ConvBnRule(320)
        self.conv4 = two_ConvBnRule(512)
        self.NCD1 = NeighborConnectionDecoder(channel)

        self.FPM1 = FPM(channel)
        self.FPM2 = FPM(channel)

        self.classifier1 = nn.Conv2d(64, 1, 1)

        self.conv_decoder = two_ConvBnRule(channel)
        self.conv_out = nn.Conv2d(64, 1, 1)


    def construct(self, x):
        # stage1-4,outs[0]是stage1的输出
        outs = self.pvt(x)

        x0_rfb = self.rfb0_1(outs[0])  # channel -> 64  # RFB模块
        # 八度卷积
        x2_rfb = self.rfb2_1(outs[1])  # channel -> 64  stage3对应的八度卷积
        x3_rfb = self.rfb3_1(outs[2])  # channel -> 64  stage4对应的八度卷积
        x4_rfb = self.rfb4_1(outs[3])  # channel -> 64  stage5对应的八度卷积

        '''Octave前后频率可视化  return x_ret, x_h, x_l, x_h_6, x_l_6!!!!!!!!!!!!!!!!!!!!'''
        # return x2_rfb
        S_g = self.NCD1(x4_rfb, x3_rfb, x2_rfb)  # (1*64*64) # 生成粗定位图
        # print('S_g:',S_g.shape)
        S_g_pre = F.interpolate(S_g, scale_factor=8., mode='bilinear',recompute_scale_factor=True)  # 插值作用于3,4维度，扩大8倍  (1*512*512)
        # print('S_g_pre:',S_g_pre.shape)
        '''粗定位结果 '''
        # return S_g_pre
        x2_ori = self.conv2(outs[1])  # channel -> 64 (64*64*64)
        x3_ori = self.conv3(outs[2])  # channel -> 64 (64*32*32)
        x4_ori = self.conv4(outs[3])  # channel -> 64 (64*16*16)

        S_g_guide4 = F.interpolate(S_g, scale_factor=0.25, mode='bilinear',recompute_scale_factor=True)  # 缩小0.25  (1*16*16)

        x3_fuse = self.FPM1(x4_ori, S_g_guide4, x3_ori) # size:16/16/32（x4_ori * S_g_guide4）*2 和 x3_ori融合
        x3_fuse_pre = self.classifier1(x3_fuse)
        S_3_pre = F.interpolate(x3_fuse_pre, scale_factor=16., mode='bilinear',recompute_scale_factor=True)   # (64,32,32) --> (64,512,512)

        x2_fuse = self.FPM2(x3_ori, x3_fuse_pre, x2_ori) # 32/32/64
        x2_fuse_up = F.interpolate(x2_fuse, scale_factor=2., mode='bilinear',recompute_scale_factor=True)  # (64,88,88)

        # 第一层特征空间注意力SA
        x0_rfb_2 = self.sa0(x0_rfb)          # x0_rfb_2 = x0_rfb       # SA消融实验****************************************
        # print('x0_rfb_2.shape:',x0_rfb_2.shape)
        # print('x2_fuse_up.shape:',x2_fuse_up.shape)
        # 相乘解码生成最终预测图
        s12 = x0_rfb_2 + x2_fuse_up

        s12 = self.conv_decoder(s12)
        s12 = self.conv_out(s12)
        s12_up1 = F.interpolate(s12, scale_factor=2., mode='bilinear', align_corners=False,recompute_scale_factor=True) 
        S12 = F.interpolate(s12_up1, scale_factor=2., mode='bilinear', align_corners=False,recompute_scale_factor=True) 

        return S_g_pre, S_3_pre, S12



if __name__ == '__main__':
    import numpy as np
    from time import time

    print("hello")
    net = FPNet(imagenet_pretrained=False,img_size=512)
    '''
    a) model.eval()，不启用 BatchNormalization 和 Dropout。
        此时pytorch会自动把BN和DropOut固定住，不会取平均，而是用训练好的值。
        不然的话，一旦test的batch_size过小，很容易就会因BN层导致模型performance损失较大；
    b) model.train() ：启用 BatchNormalization 和 Dropout。 
        在模型测试阶段使用model.train() 让model变成训练模式，
        此时 dropout和batch normalization的操作在训练q起到防止网络过拟合的问题
    '''
    # net.eval()

    # dump_x = torch.randn(1, 3, 384, 384)
    dump_x = mindspore.Tensor(np.ones([1,3,512, 512]).astype(np.float32))
    frame_rate = np.zeros((1000, 1))
    for i in range(1000):
        start = time()
        y = net(dump_x)
        end = time()
        running_frame_rate = 1 * float(1 / (end - start))
        print(i, '->', running_frame_rate)
        frame_rate[i] = running_frame_rate
    print(np.mean(frame_rate))
    print(y[0].shape,y[1].shape,y[2].shape)
